#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

freshquark

if [ ! -d ~/.quark-engine/quark-rules ]; then
    echo "freshquark failed: there is no ~/.quark-engine/quark-rules directory"
    exit 1
fi

# Run upstream tests
pytest-3

# basic test for /usr/bin/quark
quark --help
